from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from .forms import ContentForm
from .models import Page


# counter.py

class PageCounter:
    def __init__(self):
        self.count = 0

    def increment(self):
        self.count += 1
        return self.count

    def get_count(self):
        return self.count


counter: PageCounter = PageCounter()


# 1232

def index(request):
    pages = Page.objects.all()
    return render(request, 'index.html', {'item_list': pages,
                                          'count': counter.increment()})


def page(request, name):
    if request.method == 'POST':
        try:
            p = Page.objects.get(name=name)
        except Page.DoesNotExist:
            p = Page(name=name)
        form = ContentForm(request.POST)
        if form.is_valid():
            p.content = form.cleaned_data['content']
            p.save()

    if request.method == 'GET' or request.method == 'POST':
        try:
            p = Page.objects.get(name=name)
            content_form = ContentForm(initial={'content': p.content})
            status = 200
        except Page.DoesNotExist:
            content_form = ContentForm()
            status = 404
        content_template = loader.get_template('content.html')
        content_html = content_template.render({'page': name,
                                                'form': content_form},
                                               request)
        return HttpResponse(content_html, status=status)
