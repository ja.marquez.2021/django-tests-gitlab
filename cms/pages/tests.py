from django.test import TestCase
from .views import PageCounter
from django.test import Client

# Create your tests here.
class PageTest(TestCase):
    # miro si efectivamente obtengo un 200 ok
    def test_200_ok(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

class CounterTest(TestCase):
    # miro si el contador empieza en 0
    def test_star_counter(self):
        counter = PageCounter()
        self.assertEqual(counter.get_count(), 0)

    #miro que el contador incremente cada vez que cargo la pagina
    def test_increment_counter(self):
        counter = PageCounter()
        counter.increment()
        self.assertEqual(counter.get_count(), 1)

